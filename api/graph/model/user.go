package model

import (
	"time"

	"git.sr.ht/~sircmpwn/core-go/database"
)

type User struct {
	Bio              *string   `json:"bio"`
	Created          time.Time `json:"created"`
	Email            string    `json:"email"`
	ID               int       `json:"id"`
	Location         *string   `json:"location"`
	SuspensionNotice *string   `json:"suspensionNotice"`
	URL              *string   `json:"url"`
	Updated          time.Time `json:"updated"`
	UserType         UserType  `json:"userType"`
	Username         string    `json:"username"`

	PGPKeyID *int

	InternalPaymentStatus PaymentStatus
	InternalPaymentDue    *time.Time
	PaymentProcessorID    *string

	alias  string
	fields *database.ModelFields
}

func (User) IsEntity() {}

func (u *User) CanonicalName() string {
	return "~" + u.Username
}

func (u *User) As(alias string) *User {
	u.alias = alias
	return u
}

func (u *User) Alias() string {
	return u.alias
}

func (u *User) Table() string {
	return `"user"`
}

func (u *User) Fields() *database.ModelFields {
	if u.fields != nil {
		return u.fields
	}
	u.fields = &database.ModelFields{
		Fields: []*database.FieldMap{
			{"id", "id", &u.ID},
			{"created", "created", &u.Created},
			{"updated", "updated", &u.Updated},
			{"username", "username", &u.Username},
			{"email", "email", &u.Email},
			{"url", "url", &u.URL},
			{"location", "location", &u.Location},
			{"bio", "bio", &u.Bio},
			{"suspension_notice", "suspensionNotice", &u.SuspensionNotice},
			{"payment_status", "paymentStatus", &u.InternalPaymentStatus},
			{"payment_due", "paymentDue", &u.InternalPaymentDue},

			// Always fetch:
			{"id", "", &u.ID},
			{"username", "", &u.Username},
			{"user_type", "", &u.UserType},
			{"pgp_key_id", "", &u.PGPKeyID},
			{"payment_processor_id", "", &u.PaymentProcessorID},
		},
	}
	return u.fields
}

package model

import (
	"database/sql"
	"time"
	"context"

	"git.sr.ht/~sircmpwn/core-go/database"
)

type BillingInfo struct {
	FullName     *string `json:"fullName,omitempty"`
	BusinessName *string `json:"businessName,omitempty"`
	Address1     *string `json:"address1,omitempty"`
	Address2     *string `json:"address2,omitempty"`
	City         *string `json:"city,omitempty"`
	Region       *string `json:"region,omitempty"`
	Postcode     *string `json:"postcode,omitempty"`
	// ISO 3166 country code
	Country *string `json:"country,omitempty"`
	// Value-added tax number (EU)
	Vat *string `json:"vat,omitempty"`
}

type BillingSubscription struct {
	ID       int             `json:"id"`
	Created  time.Time       `json:"created"`
	Updated  time.Time       `json:"updated"`
	Active   bool            `json:"active"`
	Currency Currency        `json:"currency"`
	Interval PaymentInterval `json:"interval"`
	// Payment amount in the smallest denomination of the applicable currency (e.g.
	// cents USD). Accounts for payment interval and any applicable discount, such
	// that $10/mo USD becomes 1000 cents and $100/year USD (w/e.g. two months
	// discount) becomes 10000 cents.
	Amount int `json:"amount"`
	// If true, payment is automatically renewed when term ellapses.
	Autorenew bool `json:"autorenew"`
	// Optional billing information for invoicing.
	Billing BillingInfo `json:"billing"`

	UserID int
}

// A paid product available for purchase.
type Product struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Retired bool   `json:"retired"`
}

// Price point for a product in a given currency.
type ProductPrice struct {
	Amount   int      `json:"amount"`
	Currency Currency `json:"currency"`
}

func (p *Product) Prices(ctx context.Context) ([]ProductPrice, error) {
	var prices []ProductPrice

	if err := database.WithTx(ctx, &sql.TxOptions{
		Isolation: 0,
		ReadOnly:  true,
	}, func(tx *sql.Tx) error {
		rows, err := tx.QueryContext(ctx, `
			SELECT amount, currency
			FROM product_price
			WHERE product_id = $1
		`, p.ID)
		if err != nil {
			return err
		}

		for rows.Next() {
			var price ProductPrice
			err = rows.Scan(
				&price.Amount,
				&price.Currency,
			)
			if err != nil {
				return err
			}
			prices = append(prices, price)
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return prices, nil
}

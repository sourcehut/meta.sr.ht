package graph

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"regexp"

	"git.sr.ht/~sircmpwn/core-go/auth"
	"git.sr.ht/~sircmpwn/core-go/database"

	"git.sr.ht/~sircmpwn/meta.sr.ht/api/graph/model"
)

var usernameRE = regexp.MustCompile(`^[a-z_][a-z0-9_-]+$`)

type Resolver struct{}

type AuthorizationPayload struct {
	Grants     string
	ClientUUID string
	UserID     int
}

// Asserts that the given user is the user that authenticated for this request.
func requireAuthAs(ctx context.Context, user *model.User) error {
	if auth.ForContext(ctx).UserID != user.ID {
		return fmt.Errorf("Access denied")
	}
	return nil
}

// Populates the auth context for anonymous resolvers when the user's identity
// is established through a side channel (such as private knowledge of a token
// emailed to them)
func populateAuthCtx(ctx context.Context, user *model.User) {
	authCtx := auth.ForContext(ctx)
	if authCtx.AuthMethod != auth.AUTH_ANON_INTERNAL {
		panic(fmt.Errorf("Called populateAuthCtx in already authenticated context"))
	}

	authCtx.UserID = user.ID
	authCtx.Username = user.Username
	authCtx.Email = user.Email

	pgpKey, err := pgpKeyForUser(ctx, user)
	if err != nil {
		panic(err) // Invariant
	}

	authCtx.PGPKey = pgpKey
}

// Send a security-related notice to the authorized user.
func sendSecurityNotification(ctx context.Context, subject, details string) {
	user := auth.ForContext(ctx)
	sendSecurityNotificationTo(ctx, user.Username, user.Email,
		subject, details, user.PGPKey)
}

// Records an event in the authorized user's audit log.
func recordAuditLog(ctx context.Context, eventType, details string) {
	database.WithTx(ctx, nil, func(tx *sql.Tx) error {
		user := auth.ForContext(ctx)
		_, err := tx.ExecContext(ctx, `
			INSERT INTO audit_log_entry (
				created, user_id, ip_address, event_type, details
			) VALUES (
				NOW() at time zone 'utc',
				$1, $2, $3, $4
			);
		`, user.UserID, user.IPAddress, eventType, details)
		if err != nil {
			panic(err)
		}

		log.Printf("Audit log: %s: %s", eventType, details)
		return nil
	})
}

func pgpKeyForUser(ctx context.Context, user *model.User) (*string, error) {
	var key *string
	if user.PGPKeyID != nil {
		if err := database.WithTx(ctx, &sql.TxOptions{
			Isolation: 0,
			ReadOnly:  true,
		}, func(tx *sql.Tx) error {
			row := tx.QueryRowContext(ctx, `
				SELECT key
				FROM "pgpkey" WHERE id = $1;
				`, *user.PGPKeyID)
			if err := row.Scan(&key); err != nil {
				return err
			}
			return nil
		}); err != nil {
			return nil, err
		}
	}
	return key, nil
}

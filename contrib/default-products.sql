-- Populate the database with sample products for development purposes
INSERT INTO product (id, name)
VALUES
	(1, 'Amateur Hackers'),
	(2, 'Typical Hackers'),
	(3, 'Professional Hackers');

INSERT INTO product_price (id, product_id, currency, amount)
VALUES
	(1, 1, 'USD', 200),
	(2, 2, 'USD', 500),
	(3, 3, 'USD', 1000);

import sqlalchemy as sa
import sqlalchemy_utils as sau
from srht.database import Base
from enum import Enum

class PaymentInterval(Enum):
    monthly = "MONTHLY"
    annually = "ANNUALLY"

class PaymentCurrency(Enum):
    USD = 'USD'
    EUR = 'EUR'

class Subscription(Base):
    __tablename__ = "subscription"

    id = sa.Column(sa.Integer, primary_key=True)

    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'))
    user = sa.orm.relationship('User',
            backref=sa.orm.backref('subscriptions'),
            foreign_keys=[user_id])

    created = sa.Column(sa.DateTime, nullable=False)
    updated = sa.Column(sa.DateTime, nullable=False)
    active = sa.Column(sa.Boolean, nullable=False, server_default='t')

    currency = sa.Column(
            sau.ChoiceType(PaymentCurrency, impl=sa.String()),
            nullable=False)
    interval = sa.Column(
            sau.ChoiceType(PaymentInterval, impl=sa.String()),
            nullable=False)
    amount = sa.Column(sa.Integer, nullable=False)
    autorenew = sa.Column(sa.Boolean, nullable=False)

    full_name = sa.Column(sa.Text)
    business_name = sa.Column(sa.Text)
    address_1 = sa.Column(sa.Text)
    address_2 = sa.Column(sa.Text)
    city = sa.Column(sa.Text)
    region = sa.Column(sa.Text)
    postcode = sa.Column(sa.Text)
    country = sa.Column(sa.Text)
    vat = sa.Column(sa.Text)

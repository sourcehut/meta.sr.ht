import requests
import stripe
from datetime import datetime, timedelta
from flask import Blueprint, render_template, request, redirect
from flask import url_for, abort, Response
from metasrht.audit import audit_log
from metasrht.billing import charge_user
from metasrht.types import User, UserType, PaymentStatus, Invoice
from metasrht.types import Subscription, PaymentInterval, PaymentCurrency
from metasrht.webhooks import deliver_profile_update
from sqlalchemy import and_
from srht.config import cfg, get_origin
from srht.crypto import encrypt_request_authorization
from srht.database import db
from srht.flask import session
from srht.graphql import exec_gql, gql_time
from srht.oauth import current_user, loginrequired, freshen_user
from srht.validation import Validation

billing = Blueprint('billing', __name__)
onboarding_redirect = cfg("meta.sr.ht::settings", "onboarding-redirect")

class BillingState(dict):
    def __init__(self, amount=None):
        self.amount = amount

    def store(self):
        session["billing_state"] = self.__dict__

@billing.route("/billing")
@loginrequired
def billing_GET():
    sub = (Subscription.query
        .filter(Subscription.user_id == current_user.id)
        .filter(Subscription.active)
        .one_or_none())
    if not sub:
        return redirect(url_for(".billing_setup_GET"))

    resp = exec_gql("meta.sr.ht", """
    query Invoices {
        me {
            invoices {
                results {
                    id
                    created
                    cents
                    validThru
                    source
                }
            }
        }
    }
    """)

    invoices = resp["me"]["invoices"]["results"]
    if len(invoices) > 5:
        invoices = invoices[:5]
    for inv in invoices:
         inv["created"] = gql_time(inv["created"])
         inv["validThru"] = gql_time(inv["validThru"])

    customer = stripe.Customer.retrieve(current_user.payment_processor_id)
    methods = stripe.Customer.list_payment_methods(customer.id)

    return render_template("billing.html",
            message=session.pop("message", None),
            sub=sub, customer=customer, methods=methods,
            invoices=invoices)

@billing.route("/billing/setup")
@loginrequired
def billing_setup_GET():
    return_to = request.args.get("return_to")
    if return_to:
        session["return_to"] = return_to

    resp = exec_gql("meta.sr.ht", """
    query BillingSetup {
        me {
            invoices {
                results {
                    id
                    created
                    cents
                    validThru
                    source
                }
            }
        }

        products {
            id
            name
            prices {
                currency
                amount
            }
        }
    }
    """)

    invoices = resp["me"]["invoices"]["results"]
    if len(invoices) > 5:
        invoices = invoices[:5]
    for inv in invoices:
        inv["created"] = gql_time(inv["created"])
        inv["validThru"] = gql_time(inv["validThru"])
    products = resp["products"]
    for product in products:
        product["prices"] = {
            price["currency"]: price for price in product["prices"]
        }

    return render_template("billing-setup.html",
        message=session.pop("message", None),
        invoices=invoices, products=products)

@billing.route("/billing/setup", methods=["POST"])
@loginrequired
def billing_setup_POST():
    valid = Validation(request)
    product_id = valid.require("product_id")
    try:
        product_id = int(product_id)
    except ValueError:
        valid.error("Invalid product ID", field="product_id")
    if not valid.ok:
        return valid.response

    resp = exec_gql("meta.sr.ht", """
    query ProductDetails($productID: Int!) {
        product(id: $productID) {
            id
            name
            retired
            prices {
                currency
                amount
            }
        }
    }
    """, productID=product_id)

    product = resp["product"]
    valid.expect(product is not None, "Invalid product ID", field="product_id")
    valid.expect(not product or not product["retired"],
                 "This product has been retired",
                 field="product_id")
    if not valid.ok:
        return valid.response

    product["prices"] = {p["currency"]: p for p in product["prices"]}
    price = product["prices"]["USD"]
    amount = price["amount"]

    state = BillingState()
    state.amount = amount
    state.store()

    sub = (Subscription.query
        .filter(Subscription.user_id == current_user.id)
        .filter(Subscription.active)
        .one_or_none())
    if sub:
        return redirect(url_for("billing.billing_chperiod_GET"))

    return redirect(url_for("billing.new_payment_GET"))

@billing.route("/billing/new-payment")
@loginrequired
def new_payment_GET():
    state = session.get("billing_state")
    sub = (Subscription.query
        .filter(Subscription.user_id == current_user.id)
        .filter(Subscription.active)
        .one_or_none())

    if sub:
        amount = sub.amount
        if sub.interval == PaymentInterval.annually:
            amount /= 10
        state = BillingState()
        state.amount = amount
        state.store()
    elif state:
        state = BillingState(**state)
        amount = state.amount
    else:
        return redirect(url_for("billing.billing_setup_GET"))

    return render_template("new-payment.html", amount=amount, sub=sub)

@billing.route("/billing/new-payment", methods=["POST"])
@loginrequired
def new_payment_POST():
    valid = Validation(request)
    interval = valid.require("interval", cls=PaymentInterval)
    token = valid.require("stripe-token")
    if not valid.ok:
        return "Invalid form submission", 400

    sub = (Subscription.query
        .filter(Subscription.user_id == current_user.id)
        .filter(Subscription.active)
        .one_or_none())
    user = current_user

    state = session.pop("billing_state", None)
    if state:
        state = BillingState(**state)
        amount = state.amount
        # Apply discount for paying annually
        if interval == PaymentInterval.annually:
            amount = amount * 10
    else:
        return "Invalid form submission", 400

    # Create/update Stripe customer, add payment method
    if user.payment_processor_id:
        new_customer = False
        try:
            customer = stripe.Customer.retrieve(user.payment_processor_id)
            source = stripe.Customer.create_source(customer.id, source=token)
            customer.default_source = source.stripe_id
            customer.save()
        except stripe.error.CardError as e:
            details = e.json_body["error"]["message"]
            return render_template("new-payment.html",
                    sub=sub, amount=sub.amount, error=details)
    else:
        new_customer = True
        try:
            customer = stripe.Customer.create(
                    description="~" + current_user.username,
                    email=current_user.email,
                    card=token)
            user.payment_processor_id = customer.id
            user.payment_due = datetime.utcnow() + timedelta(minutes=-5)
        except stripe.error.CardError as e:
            details = e.json_body["error"]["message"]
            return render_template("new-payment.html",
                    sub=sub, amount=amount, error=details)

    # Add/update subscription
    if sub:
        sub.amount = amount
        sub.interval = PaymentInterval(interval)
        sub.autorenew = True

        # Immediately attempt to recharge delinquent users
        if user.payment_status == PaymentStatus.delinquent:
            user.payment_due = datetime.utcnow() + timedelta(minutes=-5)
    else:
        sub = Subscription()
        sub.user_id = user.id
        sub.created = datetime.utcnow()
        sub.updated = datetime.utcnow()
        sub.active = True
        sub.currency = PaymentCurrency.USD
        sub.amount = amount
        sub.interval = interval
        sub.autorenew = True
        db.session.add(sub)

    audit_log("billing", "New payment method added")

    success, details = charge_user(user, sub)
    if not success:
        return render_template("new-payment.html",
                sub=sub, amount=amount, error=details)

    db.session.commit()
    freshen_user()

    return_to = session.pop("return_to", None)
    if return_to:
        return redirect(return_to)

    if new_customer:
        return redirect(url_for("billing.billing_complete"))
    return redirect(url_for("billing.billing_GET"))

@billing.route("/billing/change-period")
@loginrequired
def billing_chperiod_GET():
    state = session.get("billing_state")
    if not state:
        return redirect(url_for("billing.billing_setup_GET"))
    state = BillingState(**state)

    sub = (Subscription.query
        .filter(Subscription.user_id == current_user.id)
        .filter(Subscription.active)
        .one_or_none())
    if not sub:
        return redirect(url_for("billing.billing_setup_GET"))

    return render_template("billing-change-period.html",
                           amount=state.amount, sub=sub)

@billing.route("/billing/change-period", methods=["POST"])
def billing_chperiod_POST():
    valid = Validation(request)
    interval = valid.require("interval", cls=PaymentInterval)
    if not valid.ok:
        return "Invalid form submission", 400

    state = session.pop("billing_state")
    if not state:
        return "Invalid form submission", 400

    state = BillingState(**state)
    if interval == PaymentInterval.annually:
            state.amount = state.amount * 10 # Apply yearly discount

    sub = (Subscription.query
        .filter(Subscription.user_id == current_user.id)
        .filter(Subscription.active)
        .one_or_none())
    if not sub:
        return redirect(url_for("billing.new_payment_GET"))

    sub.interval = interval
    sub.amount = state.amount
    sub.autorenew = True

    success, details = charge_user(current_user, sub)
    db.session.commit()
    freshen_user()

    audit_log("billing", "Payment details changed")

    return_to = session.pop("return_to", None)
    if return_to:
        return redirect(return_to)
    return redirect(url_for("billing.billing_GET"))

@billing.route("/billing/remove-source/<method_id>", methods=["POST"])
@loginrequired
def payment_source_remove(method_id):
    try:
        stripe.Customer.delete_source(
            current_user.payment_processor_id,
            method_id)
    except stripe.error.StripeError:
        abort(404)
    return redirect(url_for("billing.billing_GET"))

@billing.route("/billing/set-default-source/<method_id>", methods=["POST"])
@loginrequired
def payment_source_make_default(method_id):
    try:
        stripe.Customer.modify(
            current_user.payment_processor_id,
            default_source=method_id)
    except stripe.error.StripeError as ex:
        abort(404)
    return redirect(url_for("billing.billing_GET"))

@billing.route("/billing/complete")
@loginrequired
def billing_complete():
    return render_template("billing-complete.html",
            onboarding_redirect=onboarding_redirect)

@billing.route("/billing/cancel", methods=["POST"])
@loginrequired
def cancel_POST():
    sub = (Subscription.query
        .filter(Subscription.user_id == current_user.id)
        .filter(Subscription.active)
        .one_or_none())
    if not sub:
        return redirect(url_for(".billing_GET"))

    sub.autorenew = False

    # If the account is past due, apply the cancellation immediately
    if current_user.payment_status == PaymentStatus.delinquent:
        success, details = charge_user(current_user, sub)
        session["message"] = details
        assert success

    db.session.commit()

    audit_log("billing", "Payment renewal cancelled")
    return redirect(url_for("billing.billing_GET"))

@billing.route("/billing/invoices")
@loginrequired
def invoices_GET():
    cursor = request.args.get("next")

    resp = exec_gql("meta.sr.ht", """
    query Invoices($cursor: Cursor) {
        me {
            invoices(cursor: $cursor) {
                cursor
                results {
                    id
                    created
                    cents
                    validThru
                    source
                }
            }
        }
    }
    """, cursor=cursor)

    invoices = resp["me"]["invoices"]["results"]
    cursor = resp["me"]["invoices"]["cursor"]

    for inv in invoices:
        inv["created"] = gql_time(inv["created"])
        inv["validThru"] = gql_time(inv["validThru"])

    return render_template("billing-invoices.html",
        invoices=invoices, cursor=cursor)

@billing.route("/billing/invoice/<int:invoice_id>")
@loginrequired
def invoice_GET(invoice_id):
    invoice = Invoice.query.filter(Invoice.id == invoice_id).one_or_none()
    if not invoice:
        abort(404)
    if (invoice.user_id != current_user.id 
            and current_user.user_type != UserType.admin):
        abort(401)
    return render_template("billing-invoice.html", invoice=invoice)

@billing.route("/billing/invoice/<int:invoice_id>", methods=["POST"])
@loginrequired
def invoice_POST(invoice_id):
    origin = cfg("meta.sr.ht", "api-origin", default=get_origin("meta.sr.ht"))
    headers = {
        "X-Forwarded-For": ", ".join(request.access_route),
        **encrypt_request_authorization(user=current_user),
    }
    r = requests.post(f"{origin}/query/invoice/{invoice_id}",
            headers=headers, data=request.form)
    filename = f"invoice_{invoice_id}.pdf"
    headers = [('Content-Disposition', f'attachment; filename="{filename}"')]
    return Response(r.content, mimetype="application/pdf", headers=headers)

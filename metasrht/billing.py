import stripe
from datetime import datetime, timedelta
from enum import Enum
from metasrht.audit import audit_log
from metasrht.types import User, PaymentStatus, Invoice
from metasrht.types import Subscription, PaymentInterval, PaymentCurrency
from srht.config import cfg
from srht.database import db

stripe.api_key = cfg("meta.sr.ht::billing", "stripe-secret-key")

class ChargeResult(Enum):
    success = "success"
    failed = "failed"
    cancelled = "cancelled"
    delinquent = "delinquent"
    account_current = "account_current"

def charge_user(user, sub):
    if user.payment_status == PaymentStatus.free:
        return ChargeResult.account_current, "Your account is exempt from payment."

    if user.payment_due >= datetime.utcnow():
        return ChargeResult.account_current, "Your account is current."

    if user.payment_status == PaymentStatus.subsidized:
        user.payment_status = PaymentStatus.unpaid
        return ChargeResult.cancelled, "Your subsidized service term has ended. Contact support for renewal."

    assert sub is not None
    assert sub.currency == PaymentCurrency.USD # TODO

    # Temporary guardrails to ensure that we don't accidentally overcharge
    # users during the billing system upgrades
    if sub.interval == PaymentInterval.monthly:
        assert sub.amount in [200, 500, 1000]
    elif sub.interval == PaymentInterval.annually:
        assert sub.amount in [2000, 5000, 10000]

    interval = "monthly"
    if sub.interval == PaymentInterval.annually:
        interval = "annual"
    desc = f"{cfg('sr.ht', 'site-name')} {interval} payment"

    if not sub.autorenew:
        # They cancelled their payment and their current term is up
        sub.active = False
        user.payment_status = PaymentStatus.unpaid
        return ChargeResult.cancelled, "Your subscription has been cancelled."

    try:
        charge = stripe.Charge.create(
            amount=sub.amount,
            currency="usd",
            customer=user.payment_processor_id,
            description=desc)
        audit_log("billing",
                details="charged ${:.2f}".format(sub.amount / 100))
    except stripe.error.CardError as e:
        details = e.json_body["error"]["message"]
        if user.payment_status == PaymentStatus.delinquent:
            # Don't email them twice
            return ChargeResult.delinquent, "Your account payment is delinquent"
        user.payment_status = PaymentStatus.delinquent
        return ChargeResult.failed, details
    except:
        return ChargeResult.failed, "Your payment failed. Contact support."

    invoice = Invoice()
    invoice.cents = sub.amount
    invoice.user_id = user.id

    try:
        invoice.source = f"{charge.source.brand} ending in {charge.source.last4}"
    except:
        # Not a credit card? dunno how this works
        invoice.source = charge.source.stripe_id

    db.session.add(invoice)

    if sub.interval == PaymentInterval.monthly:
        invoice.valid_thru = datetime.utcnow() + timedelta(days=30)
        user.payment_due = invoice.valid_thru
    else:
        invoice.valid_thru = datetime.utcnow()
        leapday = (invoice.valid_thru.month == 2
                and invoice.valid_thru.day == 29)
        invoice.valid_thru = datetime(
                year=invoice.valid_thru.year + 1,
                month=invoice.valid_thru.month,
                day=(invoice.valid_thru.day - 1 if leapday else invoice.valid_thru.day))
        user.payment_due = invoice.valid_thru

    user.payment_status = PaymentStatus.current
    db.session.commit()
    return ChargeResult.success, "Your card was successfully charged. Thank you!"

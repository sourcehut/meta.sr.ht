"""add refresh_token_hash column to oauth2_grant

Revision ID: cd98f5cafa7c
Revises: a55e86ba3c35
Create Date: 2023-11-17 12:01:24.277051

"""

# revision identifiers, used by Alembic.
revision = 'cd98f5cafa7c'
down_revision = 'a55e86ba3c35'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("oauth2_grant", sa.Column("refresh_token_hash", sa.String(128)))


def downgrade():
    op.drop_column("oauth2_grant", "refresh_token_hash")

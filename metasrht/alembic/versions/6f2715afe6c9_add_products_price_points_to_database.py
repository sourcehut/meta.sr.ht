"""Add products & price points to database

Revision ID: 6f2715afe6c9
Revises: 851881ddabd7
Create Date: 2025-02-27 10:07:50.301377

"""

# revision identifiers, used by Alembic.
revision = '6f2715afe6c9'
down_revision = '851881ddabd7'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    CREATE TABLE product (
        id serial PRIMARY KEY,
        name text NOT NULL,
        retired boolean NOT NULL DEFAULT 'f'
    );

    CREATE TABLE product_price (
        id serial PRIMARY KEY,
        product_id integer NOT NULL REFERENCES product(id),
        currency payment_currency NOT NULL,
        amount integer NOT NULL,
        UNIQUE (product_id, currency)
    );

    -- Create products to match SourceHut upstream pricing prior to this change
    INSERT INTO product (id, name)
    VALUES
        (1, 'Amateur Hackers'),
        (2, 'Typical Hackers'),
        (3, 'Professional Hackers');

    INSERT INTO product_price (id, product_id, currency, amount)
    VALUES
        (1, 1, 'USD', 200),
        (2, 2, 'USD', 500),
        (3, 3, 'USD', 1000);

    """)


def downgrade():
    op.execute("""
    DROP TABLE product_price;
    DROP TABLE product;
    """)

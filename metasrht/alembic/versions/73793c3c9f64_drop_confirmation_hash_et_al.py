"""Drop confirmation_hash et al

Revision ID: 73793c3c9f64
Revises: be2496109d85
Create Date: 2024-12-02 14:44:39.152748

"""

# revision identifiers, used by Alembic.
revision = '73793c3c9f64'
down_revision = 'be2496109d85'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE "user"
    DROP COLUMN confirmation_hash,
    DROP COLUMN new_email,
    DROP COLUMN reset_hash,
    DROP COLUMN reset_expiry;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE "user" ADD COLUMN new_email character varying(256);
    ALTER TABLE "user" ADD COLUMN confirmation_hash character varying(128);
    ALTER TABLE "user" ADD COLUMN reset_hash character varying(128);
    ALTER TABLE "user" ADD COLUMN reset_expiry timestamp without time zone;
    """)

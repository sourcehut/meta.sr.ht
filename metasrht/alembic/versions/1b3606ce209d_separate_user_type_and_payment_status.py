"""Separate user_type and payment_status

Revision ID: 1b3606ce209d
Revises: 6d896b3a89e1
Create Date: 2024-11-08 09:51:29.268873

"""

# revision identifiers, used by Alembic.
revision = '1b3606ce209d'
down_revision = '6d896b3a89e1'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # Note: A manual touch-up step is required to convert subsidized users from
    # active_paying to payment_status = SUBSIDIZED
    op.execute("""
    CREATE TYPE user_type_new AS ENUM (
        'PENDING',
        'USER',
        'ADMIN',
        'SUSPENDED'
    );

    CREATE TYPE payment_status AS ENUM (
        'UNPAID',
        'CURRENT',
        'DELINQUENT',
        'SUBSIDIZED',
        'FREE'
    );

    CREATE TYPE payment_interval AS ENUM (
        'MONTHLY',
        'ANNUALLY'
    );

    ALTER TABLE "user"
    ADD COLUMN user_type_new user_type_new;

    ALTER TABLE "user"
    ADD COLUMN payment_status payment_status DEFAULT 'UNPAID' NOT NULL;

    UPDATE "user"
    SET user_type_new = CASE
        WHEN user_type IN (
            'ACTIVE_NON_PAYING',
            'ACTIVE_FREE',
            'ACTIVE_PAYING',
            'ACTIVE_DELINQUENT'
        ) THEN 'USER'
        WHEN user_type = 'UNCONFIRMED' THEN 'PENDING'
        WHEN user_type = 'ADMIN' THEN 'ADMIN'
        WHEN user_type = 'SUSPENDED' THEN 'SUSPENDED'
        ELSE NULL
        END::user_type_new;

    UPDATE "user"
    SET payment_status = CASE
        WHEN user_type = 'ACTIVE_NON_PAYING' THEN 'UNPAID'
        WHEN user_type = 'ACTIVE_FREE' THEN 'FREE'
        WHEN user_type = 'ACTIVE_PAYING' THEN 'CURRENT'
        WHEN user_type = 'ACTIVE_DELINQUENT' THEN 'DELINQUENT'
        ELSE 'UNPAID'
        END::payment_status;

    ALTER TABLE "user" ALTER COLUMN user_type_new SET NOT NULL;

    ALTER TABLE "user" DROP COLUMN user_type;
    ALTER TABLE "user" RENAME user_type_new TO user_type;

    DROP TYPE user_type;
    ALTER TYPE user_type_new RENAME TO user_type;

    ALTER TABLE "user"
    ALTER COLUMN payment_interval DROP DEFAULT;

    ALTER TABLE "user"
    ALTER COLUMN payment_interval
    SET DATA TYPE payment_interval
    USING CASE
        WHEN payment_status NOT IN ('CURRENT', 'DELINQUENT')
            THEN NULL
        WHEN payment_interval = 'monthly'
            THEN 'MONTHLY'::payment_interval
        ELSE 'ANNUALLY'::payment_interval
        END;
    """)


def downgrade():
    op.execute("""
    CREATE TYPE user_type_old AS ENUM (
        'UNCONFIRMED',
        'ACTIVE_NON_PAYING',
        'ACTIVE_FREE',
        'ACTIVE_PAYING',
        'ACTIVE_DELINQUENT',
        'ADMIN',
        'SUSPENDED'
    );

    ALTER TABLE "user"
    ADD COLUMN user_type_old user_type_old;

    ALTER TABLE "user"
    ALTER COLUMN payment_interval
    SET DATA TYPE character varying
    USING
        CASE WHEN payment_interval = 'MONTHLY'
            THEN 'monthly'
            ELSE 'yearly'
        END;

    ALTER TABLE "user"
    ALTER COLUMN payment_interval SET DEFAULT 'monthly';

    UPDATE "user"
    SET user_type_old = CASE
        WHEN payment_status = 'CURRENT' THEN 'ACTIVE_PAYING'
        WHEN payment_status = 'SUBSIDIZED' THEN 'ACTIVE_PAYING'
        WHEN payment_status = 'DELINQUENT' THEN 'ACTIVE_DELINQUENT'
        WHEN payment_status = 'FREE' THEN 'ACTIVE_FREE'
        WHEN user_type = 'ADMIN' THEN 'ADMIN'
        WHEN user_type = 'SUSPENDED' THEN 'SUSPENDED'
        WHEN user_type = 'PENDING' THEN 'UNCONFIRMED'
        ELSE 'ACTIVE_NON_PAYING'
        END::user_type_old;

    ALTER TABLE "user" ALTER COLUMN user_type_old SET NOT NULL;

    ALTER TABLE "user" DROP COLUMN payment_status;
    ALTER TABLE "user" DROP COLUMN user_type;
    ALTER TABLE "user" RENAME user_type_old TO user_type;

    DROP TYPE user_type;
    ALTER TYPE user_type_old RENAME TO user_type;

    DROP TYPE payment_status;
    DROP TYPE payment_interval;
    """)

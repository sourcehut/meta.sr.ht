"""Drop oauth_revocation

Revision ID: 9eb2b1a30234
Revises: cd98f5cafa7c
Create Date: 2024-10-31 10:32:00.828062

"""

# revision identifiers, used by Alembic.
revision = '9eb2b1a30234'
down_revision = 'cd98f5cafa7c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE "user" DROP COLUMN oauth_revocation_token;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE "user"
    ADD COLUMN oauth_revocation_token character varying(256);
    """)

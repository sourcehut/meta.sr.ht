"""Split billing details into separate table

Revision ID: cfe6cbf29aa9
Revises: 73793c3c9f64
Create Date: 2024-12-10 15:00:34.448062

"""

# revision identifiers, used by Alembic.
revision = 'cfe6cbf29aa9'
down_revision = '73793c3c9f64'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    CREATE TYPE payment_currency AS ENUM (
        'USD',
        'EUR'
    );

    CREATE TABLE subscription (
        id serial PRIMARY KEY,

        user_id integer NOT NULL REFERENCES "user"(id),
        created timestamp without time zone NOT NULL,
        updated timestamp without time zone NOT NULL,
        active boolean NOT NULL DEFAULT 'true',

        currency payment_currency NOT NULL,
        interval payment_interval NOT NULL,
        amount integer NOT NULL,
        autorenew boolean NOT NULL,

        full_name text,
        business_name text,
        address_1 text,
        address_2 text,
        city text,
        region text,
        postcode text,
        country text,
        vat text
    );

    INSERT INTO subscription (
        user_id,
        created,
        updated,
        currency,
        interval,
        amount,
        autorenew
    )
    SELECT
        id,
        now() at time zone 'utc',
        now() at time zone 'utc',
        'USD',
        payment_interval,
        (CASE
        WHEN payment_cents != 0 THEN payment_cents
        ELSE (
            -- payment_cents == 0 is used to signal cancellation, which is dumb.
            -- Grab payment amount from last invoice instead
            SELECT cents
            FROM invoice
            WHERE user_id = u.id
            ORDER BY created DESC
            LIMIT 1
        ) END) * (CASE WHEN payment_interval = 'MONTHLY' THEN 1 ELSE 10 END),
        payment_cents != 0
    FROM "user" u
    WHERE
        payment_status IN ('CURRENT', 'DELINQUENT') AND
        stripe_customer IS NOT NULL;

    ALTER TABLE "user"
    DROP COLUMN payment_cents,
    DROP COLUMN payment_interval;

    ALTER TABLE "user"
    RENAME stripe_customer TO payment_processor_id;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE "user"
    ADD COLUMN payment_cents integer DEFAULT 0 NOT NULL,
    ADD COLUMN payment_interval payment_interval;

    ALTER TABLE "user"
    RENAME payment_processor_id TO stripe_customer;

    WITH sub AS (
        SELECT
            sub.user_id,
            sub.amount,
            sub.interval,
            sub.autorenew
        FROM subscription sub
    )
    UPDATE "user"
    SET
        payment_cents = CASE WHEN sub.autorenew THEN sub.amount ELSE 0 END,
        payment_interval = sub.interval
    FROM sub
    WHERE id = sub.user_id;

    DROP TABLE subscription;
    DROP TYPE payment_currency;
    """)

"""add grants column to oauth2_grant

Revision ID: a55e86ba3c35
Revises: 2c272378490d
Create Date: 2023-11-01 14:09:18.613664

"""

# revision identifiers, used by Alembic.
revision = 'a55e86ba3c35'
down_revision = '2c272378490d'

from alembic import op
import sqlalchemy as sa

def column_exists(table_name, column_name):
    bind = op.get_context().bind
    insp = sa.inspect(bind)
    columns = insp.get_columns(table_name)
    return any(c["name"] == column_name for c in columns)

def upgrade():
    # column was in the Python model, but missing from schema.sql
    if not column_exists("oauth2_grant", "grants"):
        op.add_column("oauth2_grant", sa.Column("grants", sa.Unicode))


def downgrade():
    op.drop_column("oauth2_grant", "grants")

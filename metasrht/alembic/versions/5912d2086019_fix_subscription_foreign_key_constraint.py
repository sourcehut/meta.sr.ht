"""Fix subscription foreign key constraint

Revision ID: 5912d2086019
Revises: cfe6cbf29aa9
Create Date: 2025-02-14 21:51:44.182373

"""

# revision identifiers, used by Alembic.
revision = '5912d2086019'
down_revision = 'cfe6cbf29aa9'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE subscription
    DROP CONSTRAINT subscription_user_id_fkey,
    ADD CONSTRAINT subscription_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE subscription
    DROP CONSTRAINT subscription_user_id_fkey,
    ADD CONSTRAINT subscription_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id);
    """)

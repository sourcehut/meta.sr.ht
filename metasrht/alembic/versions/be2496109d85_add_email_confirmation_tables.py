"""Add email confirmation tables

Revision ID: be2496109d85
Revises: 1b3606ce209d
Create Date: 2024-11-28 15:56:29.948647

"""

# revision identifiers, used by Alembic.
revision = 'be2496109d85'
down_revision = '1b3606ce209d'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""

    CREATE TABLE user_registration (
        issued timestamp without time zone NOT NULL DEFAULT (now() at time zone 'utc'),
        user_id integer NOT NULL REFERENCES "user"(id) UNIQUE,
        token text NOT NULL
    );

    CREATE TABLE user_email_change (
        issued timestamp without time zone NOT NULL DEFAULT (now() at time zone 'utc'),
        user_id integer NOT NULL REFERENCES "user"(id) UNIQUE,
        new_email text NOT NULL,
        token text NOT NULL
    );

    CREATE TABLE user_password_change (
        issued timestamp without time zone NOT NULL DEFAULT (now() at time zone 'utc'),
        user_id integer NOT NULL REFERENCES "user"(id) UNIQUE,
        token text NOT NULL
    );

    -- Backfill pending registrations
    INSERT INTO user_registration (
        user_id,
        token
    )
    SELECT id, confirmation_hash
    FROM "user"
    WHERE user_type = 'PENDING' AND confirmation_hash IS NOT NULL;

    -- Backfill pending email changes
    INSERT INTO user_email_change (
        user_id,
        new_email,
        token
    )
    SELECT id, new_email, confirmation_hash
    FROM "user"
    WHERE
        user_type = 'USER' AND
        confirmation_hash IS NOT NULL AND
        new_email IS NOT NULL;

    -- Backfill password resets
    INSERT INTO user_password_change (
        issued,
        user_id,
        token
    )
    SELECT
        reset_expiry - interval '2 days',
        id, confirmation_hash
    FROM "user"
    WHERE
        user_type = 'USER' AND
        reset_hash IS NOT NULL AND
        reset_expiry > now() at time zone 'utc' AND
        confirmation_hash IS NOT NULL;

    """)


def downgrade():
    op.execute("""

    DROP TABLE user_registration;
    DROP TABLE user_email_change;
    DROP TABLE user_password_change;

    """)

"""Add additional cascades

Revision ID: 851881ddabd7
Revises: 5912d2086019
Create Date: 2025-02-19 11:00:39.997591

"""

# revision identifiers, used by Alembic.
revision = '851881ddabd7'
down_revision = '5912d2086019'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("""
    ALTER TABLE user_registration
    DROP CONSTRAINT user_registration_user_id_fkey,
    ADD CONSTRAINT user_registration_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;

    ALTER TABLE user_email_change
    DROP CONSTRAINT user_email_change_user_id_fkey,
    ADD CONSTRAINT user_email_change_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;

    ALTER TABLE user_password_change
    DROP CONSTRAINT user_password_change_user_id_fkey,
    ADD CONSTRAINT user_password_change_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id) ON DELETE CASCADE;
    """)


def downgrade():
    op.execute("""
    ALTER TABLE user_registration
    DROP CONSTRAINT user_registration_user_id_fkey,
    ADD CONSTRAINT user_registration_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id);

    ALTER TABLE user_email_change
    DROP CONSTRAINT user_email_change_user_id_fkey,
    ADD CONSTRAINT user_email_change_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id);

    ALTER TABLE user_password_change
    DROP CONSTRAINT user_password_change_user_id_fkey,
    ADD CONSTRAINT user_password_change_user_id_fkey
    FOREIGN KEY (user_id) REFERENCES "user"(id);
    """)
